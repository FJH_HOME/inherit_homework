package JavaBase.Inheritance_HomeWork;

/*
（2）Employee类中的属性有：办公室office（String类型），工资salary（double类型），受雇日期hiredate（String类型）；

 */

public class Employee extends Person{

    private String office;
    private double salary;
    private String hiredate;

    public Employee(String name,String address,String telphone,String email,String office,double salary,String hiredate)
    {
        super(name,address,telphone,email);
        this.office=office;
        this.salary=salary;
        this.hiredate=hiredate;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public void printShow()
    {
        System.out.println("姓名："+this.getName());
        System.out.println("家庭住址："+this.getAddress());
        System.out.println("电话："+this.getTelphone());
        System.out.println("邮箱："+this.getEmail());
        System.out.println("办公室："+this.getOffice());
        System.out.println("工资："+this.getSalary());
        System.out.println("受雇日期："+this.getHiredate());
    }


}

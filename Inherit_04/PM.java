package JavaBase.Inheritance_HomeWork;
/*
（4）PM类中的属性有：管理经验yearsOfExperience（String类型）。
 */
public class PM extends Employee{

    private String yearsOfExperience;

    public PM(String name,String address,String telphone,String email,String office,double salary,String hiredate,
              String yearsOfExperience)
    {
        super(name,address,telphone,email,office,salary,hiredate);
        this.yearsOfExperience=yearsOfExperience;
    }
    public String getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }


    public void printShow() {
        System.out.println("***项目经理的自我介绍***");
        super.printShow();
        System.out.println("管理经验："+this.yearsOfExperience);
    }
}

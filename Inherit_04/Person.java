package JavaBase.Inheritance_HomeWork;
/*
上机作业4：实现一个名为Person的类和它的子类Employee，Employee有两个子类SE(软件工程师类)和PM（项目经理类）。

具体要求如下：
（1）Person类中的属性有：姓名name（String类型），地址address（String类型），电话号码telphone（String类型）和电子邮件地址email（String类型）；
（2）Employee类中的属性有：办公室office（String类型），工资salary（double类型），受雇日期hiredate（String类型）；
（3）SE类中的属性有：爱好hobby（String类型），级别level（String类型）例如：P5/P6/P7/P8/P9/P10；
（4）PM类中的属性有：管理经验yearsOfExperience（String类型）。

 */
public abstract class Person {

    private String name;
    private String address;
    private String telphone;
    private String email;

    public Person(String name,String address,String telphone,String email)
    {
        this.name=name;
        this.address=address;
        this.telphone=telphone;
        this.email=email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public abstract  void printShow();
}

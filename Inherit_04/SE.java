package JavaBase.Inheritance_HomeWork;
/*
（3）SE类中的属性有：爱好hobby（String类型），级别level（String类型）例如：P5/P6/P7/P8/P9/P10；

 */

public class SE extends Employee{

    private String hobby;
    private String level;

    public SE(String name,String address,String telphone,String email,String office,double salary,String hiredate,
    String hobby,String level)
    {
        super(name,address,telphone,email,office,salary,hiredate);
        this.hobby=hobby;
        this.level=level;

    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public void printShow() {
        System.out.println("***软件工程师的自我介绍***");
        super.printShow();
        System.out.println("爱好："+this.hobby);
        System.out.println("级别："+this.level);

    }
}

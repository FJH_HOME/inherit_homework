package JavaBase.Inheritance_HomeWork;

public class Person_Test {
    public static void main(String[] args) {
        Person Zhang;
        Person Li;
        Zhang=new PM("小张","合肥市屯溪路","123456","234@qq.com","北大青鸟咨询室"
        ,5000,"2012年2月2号","企业框架");
        Li=new SE("小李","合肥市屯溪路","234567","345@qq.com","北大青鸟咨询室"
                ,5500,"2013年3月4号","篮球","P4");
        Zhang.printShow();
        Li.printShow();
    }
}

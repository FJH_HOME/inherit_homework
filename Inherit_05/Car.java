package JavaBase.Inheritance_HomeWork;

public abstract class Car {

    private String brand;
    private double price;
    private String model;

    public Car(String brand,double price,String model)
    {
        this.brand=brand;
        this.price=price;
        this.model=model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void drive()
    {
        System.out.println(this.brand+"汽车驾驶中~~~");
    }
    public abstract void variable_speed();
}

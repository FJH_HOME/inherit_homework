package jicheng;

public class Teacher {
	
	private String name;
	private String gender;
	private int age;
	private int school_age;
	private String center;
	
	public Teacher(String name,String gender,int age,int school_age,String center)
	{
		this.name=name;
		this.gender=gender;
		this.age=age;
		this.school_age=school_age;
		this.center=center;
	}
	
	public void teaching()
	{
		System.out.println("实施理论课授课");
	}
	
	public void printShow()
	{
		System.out.println("姓名："+this.name);
		System.out.println("性别："+this.gender);
		System.out.println("年龄："+this.age);
		System.out.println("教龄："+this.school_age);
		System.out.println("所在中心："+this.center);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSchool_age() {
		return school_age;
	}

	public void setSchool_age(int school_age) {
		this.school_age = school_age;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}
	
	

}

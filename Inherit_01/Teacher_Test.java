package jicheng;

public class Teacher_Test {

	public static void main(String[] args) {
		Teacher zhangTeacher;
		Teacher liTeacher;
		zhangTeacher=new Java_Teacher("张老师","男",32,7,"北大青鸟科海学院");
		zhangTeacher.printShow();
		zhangTeacher.teaching();
		
		liTeacher=new Net_Teacher("李老师","女",28,4,"北大青鸟科海学院");
		liTeacher.printShow();
		liTeacher.teaching();
	}

}
